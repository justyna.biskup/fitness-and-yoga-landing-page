$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top-40
        }, 1000);
        return false;
      }
    }
  });
});


$(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
});


$(document).ready(function(){
   $('.slider_for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    asNavFor: '.slider_nav'
  });
  $('.slider_nav').slick({
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider_for',
    centerMode: true,
    adaptiveHeight: true,
    nextArrow: $('.myNext'),
    prevArrow: $('.myPrev'),
  });

});

$(".emptyLink").click(function(e){
  e.preventDefault();
})